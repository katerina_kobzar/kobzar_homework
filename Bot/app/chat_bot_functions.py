import telebot

from app.data_types import Currency
from app.init_domain import currency_rates
from app.chat_bot_view import bot
from app.utils import save_conversion


def main_menu(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.row('Конвертувати валюту', 'Допомога', 'Вийти', 'Історія')
    bot.send_message(message.chat.id, 'Оберіть опцію:', reply_markup=keyboard)


def process_base_currency(message):
    base_currency = message.text.upper()
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2,
                                               one_time_keyboard=True,
                                               resize_keyboard=True)
    btn1 = telebot.types.KeyboardButton(Currency.usd)
    btn2 = telebot.types.KeyboardButton(Currency.eur)
    btn3 = telebot.types.KeyboardButton('UAH')
    markup.add(btn1, btn2, btn3)
    msg = bot.send_message(message.chat.id,
                           f'Виберіть в яку валюту конвертувати {base_currency}:',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, lambda m: process_target_currency(m,
                                                                          base_currency))


def process_target_currency(message, base_currency):
    target_currency = message.text.upper()
    msg = bot.send_message(message.chat.id,
                           f'Введіть суму для конвертації з {base_currency} в {target_currency}:')
    bot.register_next_step_handler(msg,
                                   lambda m: convert_currency(m, base_currency,
                                                              target_currency))


def convert_currency(message, base_currency, target_currency):
    amount = float(message.text)
    converted_amount = currency_rates.convert_currency(amount, base_currency,
                                                       target_currency)
    bot.send_message(message.chat.id,
                     f'{amount} {base_currency} = {converted_amount:.2f} {target_currency}')
    conversion_data = {
        "amount": amount,
        "base_currency": base_currency,
        "target_currency": target_currency,
        "converted_amount": f"{converted_amount:.2f}"
    }
    save_conversion(conversion_data=conversion_data, items_count=10,
                    path_to_file="conversions.json")
    bot.reply_to(message,
                 'Вітаю! Я бот для конвертації валют. Ви можете ввести суму та '
                 'вихідну валюту у форматі "100 USD", після чого виберіть валюту'
                 ' для конвертації. '
                 'Наприклад, "100 UAH", або натисніть кнопку')

    main_menu(message)


def handle_target_currency(message, amount, from_currency):
    to_currency = message.text.upper()
    converted_amount = currency_rates.convert_currency(amount, from_currency,
                                                       to_currency)
    bot.send_message(message.chat.id,
                     f"{amount} {from_currency} = {converted_amount:.2f} {to_currency}")
    conversion_data = {
        "amount": amount,
        "base_currency": from_currency,
        "target_currency": to_currency,
        "converted_amount": f"{converted_amount:.2f}"
    }
    save_conversion(conversion_data, 'conversions.json')