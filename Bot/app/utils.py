import json
import os


def save_conversion(conversion_data: dict, path_to_file: str,
                    items_count: int = 10, indent: int = 4) -> None:
    """Stores 'conversion_data' in the specified JSON file and ensures the file
    contains only the last 'items_count' records.

   :param conversion_data:The conversion data to save
   :param path_to_file: File path
   :param items_count: The number of most recent records to keep in the file.
   :param indent:The number of spaces to use as indentation in the JSON file
   :return: None
   """
    if os.path.exists(path_to_file):
        with open(path_to_file, 'r') as file:
            data = json.load(file)
    else:
        data = []

    data.append(conversion_data)

    if len(data) > items_count:
        data = data[items_count:]

    with open(path_to_file, 'w') as file:
        json.dump(data, file, indent=indent)
