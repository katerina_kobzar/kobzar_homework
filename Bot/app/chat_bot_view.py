import json

import telebot

from app.init_domain import bot
from app.chat_bot_functions import main_menu, handle_target_currency, \
    process_base_currency


@bot.message_handler(commands=['start', 'help', 'hello'])
def handle_command(message):
    if message.text == '/hello':
        bot.reply_to(message, 'Привіт! Вітаю вас.')
    elif message.text == '/help' or message.text == '/start':
        bot.reply_to(message,
                     'Вітаю! Я бот для конвертації валют. Ви можете ввести '
                     'суму та вихідну валюту у форматі "100 USD", після чого '
                     'виберіть валюту для конвертації. '
                     'Наприклад, "100 UAH", або натисніть кнопку')

        main_menu(message)


@bot.message_handler(func=lambda message: message.text == 'Історія')
def show_history(message):
    with open('conversions.json', 'r') as file:
        data = json.load(file)
    good_data = json.dumps(data, indent=4)
    good_data = good_data.replace('{', '').replace('}', '')
    bot.reply_to(message, f"История:\n\n{good_data}")


@bot.message_handler(func=lambda message: message.text == 'Допомога')
def show_help(message):
    bot.reply_to(message,
                 'Введіть суму та вихідну валюту у форматі "100 USD", після чого '
                 'виберіть валюту для конвертації. Наприклад, "100 UAH".')


@bot.message_handler(func=lambda message: message.text == 'Вийти')
def show_exit(message):
    bot.reply_to(message, 'До зустрічі!')
    bot.stop_polling()


@bot.message_handler(func=lambda message: message.text == 'Конвертувати валюту')
def ask_amount_and_currency(message):
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2,
                                               one_time_keyboard=True,
                                               resize_keyboard=True)
    btn1 = telebot.types.KeyboardButton('USD')
    btn2 = telebot.types.KeyboardButton('EUR')
    btn3 = telebot.types.KeyboardButton('UAH')
    markup.add(btn1, btn2, btn3)
    msg = bot.send_message(message.chat.id,
                           'Виберіть з якої валюти конвертувати:',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, process_base_currency)


@bot.message_handler(func=lambda message: True)
def handle_conversion(message):
    parts = message.text.split()
    amount = float(parts[0])
    from_currency = parts[1].upper()
    keyboard = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True,
                                                 resize_keyboard=True)
    keyboard.row('USD', 'EUR', 'UAH')
    bot.send_message(message.chat.id,
                     'Виберіть цільову валюту для конвертації:',
                     reply_markup=keyboard)
    bot.register_next_step_handler(message, handle_target_currency, amount,
                                   from_currency)


import_placeholder = None
