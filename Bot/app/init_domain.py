import telebot

from domain import CurrencyRates

with open("../.env", "r") as file:
    TBOT_TOKEN = file.read().strip()
bot = telebot.TeleBot(TBOT_TOKEN)

currency_rates = CurrencyRates()

