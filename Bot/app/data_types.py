from enum import StrEnum


class Currency(StrEnum):
    usd = "USD"
    eur = "EUR"
