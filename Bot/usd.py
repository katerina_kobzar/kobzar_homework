import requests
import json


class CurrencyRates:
    def __init__(self):
        self.rates_data = self._get_rates_data()

    def _get_rates_data(self):
        url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            response.raise_for_status()

    def get_rate(self, currency_code):
        for rate in self.rates_data:
            if rate['cc'] == currency_code:
                return rate['rate']
        return None

    def get_text(self):
        result = "Exchange rates:\n"
        for rate in self.rates_data:
            result += f"{rate['txt']} ({rate['cc']}): {rate['rate']} UAH\n"
        return result

    def __str__(self):
        return self.get_text()

    def convert_currency(self, amount, from_currency, to_currency):
        if from_currency == 'UAH':
            rate_to = self.get_rate(to_currency)
            if rate_to:
                return amount / rate_to
        elif to_currency == 'UAH':
            rate_from = self.get_rate(from_currency)
            if rate_from:
                return amount * rate_from
        else:
            rate_from = self.get_rate(from_currency)
            rate_to = self.get_rate(to_currency)
            if rate_from and rate_to:
                return amount * (rate_from / rate_to)
        return None


def main():
    currency_rates = CurrencyRates()

    while True:
        amount = float(input("Введіть сумму: "))
        from_currency = input("Введіть валюту для конвертаціі: ").upper()
        to_currency = input("Введіть валюту в яку хочете конвертувати: ").upper()

        converted_amount = currency_rates.convert_currency(amount, from_currency, to_currency)
        if converted_amount is not None:
            print(f"{amount} {from_currency} всього {converted_amount:.2f} {to_currency}")
        else:
            print(f"Конвертація {from_currency} у {to_currency} неможлива при данних значеннях,"
                  f" введіть корректний код валют.")

main()
