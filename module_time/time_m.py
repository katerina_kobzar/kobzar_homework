import time


def generate_odd_numbers():
    nums = list(range(n + 1))

    nums[1] = 0

    primes = []

    for i in nums:
        if i > 1:
            for j in range(i + i, len(nums), i):
                nums[j] = 0

            primes.append(i)


numbers = [1000, 10000, 100000]
start = time.time()
for n in numbers:
    generate_odd_numbers()
    print(f"Odd numbers up to {n} ")
    end = time.time()
    execution_time = end - start
    print(f"Execution time: {execution_time} seconds")

