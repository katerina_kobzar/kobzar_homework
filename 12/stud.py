all_student2 = [
    (84, "Passed"),
    (78, "Passed"),
    (65, "Filled"),
    (90, "Passed"),
    (72, "Filled")
]
all_student3 = [
    (82, "Passed"),
    (78, "Passed"),
    (97, "Passed"),
    (86, "Passed"),
    (67, "Passed"),
    (75, "Passed")
]


def student_const(consistent):
    for student in all_student3:
        consistent = student[0]
        grate = student[1]
        if consistent >= 73 and grate == "Filled":
            print(f"{consistent} is not filled")
        if consistent < 73 and grate == "Passed":
            print(f"{consistent} is not passed")
    # print("Professor is not consistent")
    return consistent


def student_reference(grate_student):
    grater_failed = 0
    for student in all_student2:
        grate_student = student[1]
        grade = student[0]
        if grate_student == "Filled" and grade > grater_failed:
            grater_failed = grade + 1

    grater_failed2 = 100
    for student in all_student2:
        grate_student = student[1]
        grade = student[0]
        if grate_student == "Passed" and grade < grater_failed2:
            grater_failed2 = grade
    print(f"Exam range: {grater_failed} - {grater_failed2}")
    return grater_failed


is_consistent = student_const(all_student3)
if is_consistent:
    is_reference = student_reference(all_student2)
else:
    print(f"{is_consistent} is not consistent")
