from collections import Counter

products_list = []
with open("orders.txt", "r") as file:
    for line in file:
        products = line.strip("\n").split("@@@")
        products_list.extend(products)

product_counts = Counter(products_list)

for product, count in product_counts.items():
    print(f"{product}: {count}")
